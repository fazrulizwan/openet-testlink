package com.openet.testlinkclient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Vector;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.TestLinkAPIException;
import br.eti.kinoshita.testlinkjavaapi.model.ExecutionType;
import br.eti.kinoshita.testlinkjavaapi.model.Platform;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestCaseStep;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.model.TestProject;
import br.eti.kinoshita.testlinkjavaapi.model.TestSuite;

public class TestLinkClient {
		    
    private TestLinkAPI api = null;
    private URL testlinkURL = null;   
    
	
	public int createTestCase(String caseName, int suiteID, int projID, String loginName, String summary, String strExecType, int order, List<TestCaseStep> stepList) {
		
		TestCase tc = null;
		ExecutionType execType = null;
		
		if(api == null)
			return -3; //** connect error
		
		if(strExecType.equalsIgnoreCase("AUTOMATED"))
			execType = ExecutionType.AUTOMATED;
		else if(strExecType.equalsIgnoreCase("MANUAL"))
			execType = ExecutionType.MANUAL;
		
		try {
			if(stepList.size() == 0) //** no step defined
				tc = api.createTestCase(caseName, suiteID, projID, loginName, summary, null, null, null, execType, order, null, true, "block");
			else
				tc = api.createTestCase(caseName, suiteID, projID, loginName, summary, stepList, null, null, execType, order, null, true, "block");
		} catch(TestLinkAPIException e) {
			return -4; //** general exception. 
		}
		return tc.getId();
				
	}
	
	public int connect(String serverURL, String devKey) {
		
		try {
			testlinkURL = new URL(serverURL);
			api = new TestLinkAPI(testlinkURL, devKey);			
		} catch ( MalformedURLException mue ) {
			mue.printStackTrace();		
			return -1;
		}
		
		return 0;
	}
	
	public int createTestCase(String caseName, int suiteID, int projID, String loginName, String summary, String strExecType, String orderOption, List<TestCaseStep> stepList) {
		
		TestCase[] cases = null;	
		
		if(api == null)
			return -3; //** connect error
				
		if(orderOption.equalsIgnoreCase("ALWAYS_ONTOP")) {
			return createTestCase(caseName, suiteID, projID, loginName, summary, strExecType, -1, stepList);
		}
		else if(orderOption.equalsIgnoreCase("ALWAYS_APPEND")) {
			cases = api.getTestCasesForTestSuite(suiteID, null, null);
			return createTestCase(caseName, suiteID, projID, loginName, summary, strExecType, cases.length, stepList);
		}
		else {
			return -1;
		}
			
	}
	
	public int getProjectID(String projName) {
		
		TestProject proj = null;
		
		if(api == null)
			return -3; //** connect error
		
		try {
			proj = api.getTestProjectByName(projName);
			return proj.getId();
		} catch (TestLinkAPIException tlae) {
			//System.out.println("ERROR: the project name may not exist");
			return -2; //** not found
		}
	}
	
	public int getTestSuiteIDByName (int projID, String suiteName) {
		
		TestSuite[] suites = null;
		
		if(api == null)
			return -3; //** connect error
								
		suites = api.getFirstLevelTestSuitesForTestProject(projID);
		for(int i=0;i<suites.length;i++) {
			//System.out.println(suites[i].getName() + " " + suites[i].getId());
			if(suiteName.equalsIgnoreCase(suites[i].getName())) {
				return suites[i].getId();
			}
		}			
				
		return -2; //** not found
	}
	
	public int getTestSuiteIDByName (int projID, Vector<String> suiteHier, int hierIndex, String details, boolean isLookUpMode) {
		
		/*
		 * variant for non-top level node
		 * if isLookUpMode is true, it won't create any node, the lookup for suite ID will dig up to all nodes
		 * Returns the parent's node id
		 */
		
		TestSuite[] suites = null;
		int parentSuiteId = 0;			
		
		if(api == null)
			return -3; //** connect error
		
		if(hierIndex == 0) { //** the topmost node reached
			parentSuiteId = getTestSuiteIDByName(projID, suiteHier.get(hierIndex));
			if((parentSuiteId == -2) && (isLookUpMode==false)) { //** parent node does not exist
				parentSuiteId = createTestSuite(projID, suiteHier.get(hierIndex), details, "ALWAYS_APPEND"); //** create self as topmost node
			}
			return parentSuiteId;
		}
		else { //** not the topmost node
			parentSuiteId = getTestSuiteIDByName(projID, suiteHier, hierIndex - 1, details, isLookUpMode);
			if(parentSuiteId < 0) //** guard
				return parentSuiteId;
			
			if(! isLookUpMode) { //** creation mode exits earlier to return parent id			
				if(hierIndex == (suiteHier.size() - 1)) //** if this is the last stack item
					return parentSuiteId;
			}
			
			try {
				suites = api.getTestSuitesForTestSuite(parentSuiteId);
			} catch(TestLinkAPIException e) { //** no specific exception here, but if a node doesnt have any child exception is thrown
				if(isLookUpMode)
					return -2; //** not exist
				else
					parentSuiteId = createChildTestSuite(projID, suiteHier.get(hierIndex), parentSuiteId, details, 1000); //** create self
			}
			if((suites.length == 0) && (isLookUpMode==false)) { //** the parent suite has no child
				parentSuiteId = createChildTestSuite(projID, suiteHier.get(hierIndex), parentSuiteId, details, 1000); //** create self
			} else { //** the parent suite has child
				for(int i = 0; i < suites.length; i++) {
					if(suites[i].getName().equalsIgnoreCase(suiteHier.get(hierIndex))) { //** see if child match
						return suites[i].getId();
					}
				}
				if(isLookUpMode)
					return -2; //** not exist
				else
					parentSuiteId = createChildTestSuite(projID, suiteHier.get(hierIndex), parentSuiteId, details, 1000); //** create self
			}
			return parentSuiteId;
		}
		
		
	}
	
	public int createTestSuite(int projID, String suiteName, String details, int order) {
		
		TestSuite suite = null;
		
		if(api == null)
			return -3; //** connect error
		
		try {
			if(order == -1)
				suite = api.createTestSuite(projID, suiteName, details, null, null, true, null); //todo buggy it's not ontop
			else
				suite = api.createTestSuite(projID, suiteName, details, null, order, true, "block");
			return suite.getId();
		} catch (TestLinkAPIException tlae) {
			tlae.printStackTrace();
			//System.out.println("code: " + tlae.getCode());
			return -2; //** not found
		}
	}	
	
	public int createTestSuite(int projID, String suiteName, String details, String orderOption) {
		
		TestSuite[] suites = null;
		
		if(api == null)
			return -3; //** connect error
		
		try {
			if(orderOption.equalsIgnoreCase("ALWAYS_ONTOP")) {
				return createTestSuite(projID, suiteName, details, -1);
			}
			else if(orderOption.equalsIgnoreCase("ALWAYS_APPEND")) {
				suites = api.getFirstLevelTestSuitesForTestProject(projID);
				return createTestSuite(projID, suiteName, details, suites.length);
			}
			else {
				return -1;
			}
		} catch (TestLinkAPIException tlae) {
			tlae.printStackTrace();
			return -2; //*** not found
		}
	}	
	
	public int createChildTestSuite(int projID, String childSuiteName, int parentSuiteId, String details, int order) {
		
		TestSuite suite = null;		
		
		if(api == null)
			return -3; //** connect error			
		
		try {
			if(order == -1)
				suite = api.createTestSuite(projID, childSuiteName, details, parentSuiteId, null, true, null); //todo buggy it's not ontop
			else
				suite = api.createTestSuite(projID, childSuiteName, details, parentSuiteId, order, true, "block");
			return suite.getId();
		} catch (TestLinkAPIException tlae) {
			tlae.printStackTrace();
			//System.out.println("code: " + tlae.getCode());
			return -2; //** not found
		}				
	}
	
	public int createChildTestSuite(int projID, String childSuiteName, String parentSuiteName, Vector<String> suiteHier, int hierIndex, String details, int order) {
				
		int parentSuiteId = -1;
		
		if(api == null)
			return -3; //** connect error
		
		parentSuiteId = getTestSuiteIDByName(projID, suiteHier, hierIndex, details, false); //** get the parent ID, create parent if need be
		if(parentSuiteId == -2) //** parent suite does not exist
			return -4;
		
		try {			
			return createChildTestSuite(projID, childSuiteName, parentSuiteId, details, order);				
		} catch (TestLinkAPIException tlae) {
			tlae.printStackTrace();
			//System.out.println("code: " + tlae.getCode());
			return -2; //** not found
		}		
	}
	
	public int createChildTestSuite(int projID, String childSuiteName, String parentSuiteName, Vector<String> suiteHier, int hierIndex, String details, String orderOption) {	
		
		if(api == null)
			return -3; //** connect error
		
		try {
			if(orderOption.equalsIgnoreCase("ALWAYS_ONTOP")) {
				return createChildTestSuite(projID, childSuiteName, parentSuiteName, suiteHier, hierIndex, details, -1);
			}
			else if(orderOption.equalsIgnoreCase("ALWAYS_APPEND")) {
				
				return createChildTestSuite(projID, childSuiteName, parentSuiteName, suiteHier, hierIndex, details, 1000); //** set 1000 to always in the ends
			}
			else {
				return -1;
			}
		} catch (TestLinkAPIException tlae) {
			tlae.printStackTrace();
			return -2; //*** not found
		}
	}
	
	private int getTestSuiteID(int projID, String suiteName, String details, boolean createIfNotExist) {
		
		/**
		 * 		- Specifically for topmost node -
		 * 		Get Test Suite ID for the given suite name.
		 * 		If Test Suite doesn't exist, it will create one provided createIfNotExist==true
		 *		 
		 * 
		 * 		return Test Suite ID (> 0)
		 * 		return error (< 0) -2 not exist
		 */
		
		int status = 0;
		int suiteID;
		
		suiteID = getTestSuiteIDByName(projID, suiteName);
		//System.out.println("Suite ID: " + suiteID);
		if(suiteID == -2) { //** test suite does not exist
			if(createIfNotExist) {
				status = createTestSuite(projID, suiteName, details, "ALWAYS_APPEND");
				//System.out.println("Status of create suite: " + status);
				if(status == -2) {
					//System.out.println("Test Suite already exist when try to create one");
				} else if(status < 0) {
					//System.out.println("General error when try to create a test suite");
				}	
				return status;	//** will return as status if suite just been created	
			}
		} 	
		
		return suiteID; //** will return suiteID, if suite already exist
	}
	
	private int getTestSuiteID(int projID, String suiteName, boolean createIfNotExist) {
		return getTestSuiteID(projID, suiteName, "", createIfNotExist);
	}	
	
	private int getTestSuiteID(int projID, String suiteName, String details, boolean createIfNotExist, String parentSuiteName, Vector<String> suiteHier, int hierIndex) {
		
		/**		
		 * 		- specifically for child node -
		 * 		Get Test Suite ID for the given suite name.
		 * 		If Test Suite doesn't exist, it will create one provided createIfNotExist==true
		 * 		
		 * 
		 * 		return Test Suite ID (> 0)
		 * 		return error (< 0) -2 not exist, -4 parent not exist
		 */
		
		int status = 0;
		int suiteID;				
		
		suiteID = getTestSuiteIDByName(projID, suiteHier, hierIndex, details, true); //** lookup mode
		
		if(suiteID == -2) { //** test suite does not exist
			if(createIfNotExist) {				
				status = createChildTestSuite(projID, suiteName, parentSuiteName, suiteHier, hierIndex, details, "ALWAYS_APPEND");
				if(status == -4) //*** parent not exist
					return -4;
				else if(status == -2) {
					//**blank
				} else if(status < 0) {
					//**blank
				}	
				return status;	//** will return as status if suite just been created	
			}
		} 	
		
		return suiteID; //** will return suiteID, if suite already exist
	}
	
	private int getTestSuiteID(int projID, String suiteName, boolean createIfNotExist, String parentSuiteName, Vector<String> suiteHier, int hierIndex) {
		return getTestSuiteID(projID, suiteName, "", createIfNotExist, parentSuiteName, suiteHier, hierIndex);
	}
	
	public int getTestSuiteID(int projID, Vector<String> suiteHier, boolean createSuiteIfNotExist) {
		
		int suiteID = 0;
		int suiteIndex = 0;
		boolean keepDoing = true;
		
		if(suiteHier.size() == 1) { //** no child, the only suite
			suiteID = getTestSuiteID(projID, suiteHier.get(0), createSuiteIfNotExist);
		}
		else if(suiteHier.size() > 1) { //** have child
			suiteIndex = suiteHier.size() - 1; //** to work from the last
			while(keepDoing) {
				if(suiteIndex == 0) //** working on the topmost node, no parent
					suiteID = getTestSuiteID(projID, suiteHier.get(suiteIndex), createSuiteIfNotExist);
				else if(suiteIndex > 0) //** working on the child node, have parent
					suiteID = getTestSuiteID(projID, suiteHier.get(suiteIndex), createSuiteIfNotExist, suiteHier.get(suiteIndex - 1), suiteHier, suiteIndex);
				else
					suiteID = -1;
				
				if(suiteID == -4) {	//** parent not exist
					suiteIndex--; //** get one level up to create the upper node
				}
				else if(suiteID > 0) { //** suite got created
					suiteIndex++;
				}
				
				if(suiteIndex >= suiteHier.size()) //** done
					keepDoing = false;
			}
		}
		
		return suiteID;
	}
	
	public int addTestCaseToTestPlan(int projId, int testPlanId, int testCaseId, Integer platformId, int order) {
		
		int status = 0;
		
		if(api == null)
			return -3; //** connect error
								
		status = api.addTestCaseToTestPlan(projId, testPlanId, testCaseId, 1, platformId, 0, null);					
				
		return status;
	}
	
	public int getProjectPlanByName(int projId, String projPlanName) {
		
		if(api == null)
			return -3; //** connect error
		
		TestPlan[] testPlan = null;
		
		testPlan = api.getProjectTestPlans(projId);
		for(int i = 0; i < testPlan.length; i++) {
			if(testPlan[i].getName().equalsIgnoreCase(projPlanName))
				return testPlan[i].getId();
		}
		
		return -2; //** not found
	}	
	
	public int createTestPlan(String planName, String projName) {
		
		if(api == null)
			return -3; //** connect error
		
		TestPlan plan = api.createTestPlan(planName, projName, "Notes about testplan", false, false);
		
		return plan.getId();
		
	}
	
	public Integer getPlatform(int planId, String platformName) {		
		
		if(api == null)
			return -3; //** connect error
		
		Platform[] platform = null;
		
		platform = api.getTestPlanPlatforms(planId);
		
		for(int i=0; i<platform.length; i++) {
			if(platform[i].getName().equalsIgnoreCase(platformName))
				return platform[i].getId();
		}
		
		return -2; //** not found
	}
	
}
