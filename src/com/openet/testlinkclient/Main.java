package com.openet.testlinkclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Vector;

import br.eti.kinoshita.testlinkjavaapi.model.ExecutionType;
import br.eti.kinoshita.testlinkjavaapi.model.TestCaseStep;

public class Main {
	
	private static String argVal = "";	
	private static String atfFile = "";
	private static String devKey = "";   
    private static String serverURL = ""; 
    private static boolean isShowConfirm = true;
    private static boolean createSuiteIfNotExist = true;
    private static boolean proceedExportIfHaveError = true;
    private static String projName = "";
    private static String loginName = "";
    private static String logFile = "";
    private static String suiteName = "";
    private static boolean ATFOverideTestSuiteName = true;
    
    //*** test case table settings    
    private static int indexColumnTrunc = 5;
    private static int execColumnTrunc = 10;
    private static int titleColumnTrunc = 40;
    private static int summaryColumnTrunc = 90;
    private static int suiteColumnTrunc = 35;
    private static int noStepColumnTrunc = 5;
    
    //** test step table settings
    private static int tsIndexColumnTrunc = 5;
    private static int stepColumnTrunc = 80;
    private static int expectedColumnTrunc = 85;
    private static int tsExecColumnTrunc = 10;
    
    //** universal table settings
    private static int tablePadding = 3;
    
    static BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
    
    private static String getArg(String[] args, String name, String defaultValue) {

        String nameCopy = name.toLowerCase();

        for (int i=0; i<args.length; i++)
        {
            String arg = args[i].toLowerCase();

            if (arg.startsWith("-"+nameCopy+"="))
            {
                return args[i].substring(name.length()+2);
            }
        }
        return defaultValue;
    }
    
    private static void educate(String message) {
    	display("*********************************************************");
		display("*	ATF -> TestLink Test Case Exporter Tool				*");
		display("*********************************************************");
		display("Option error: " + message);
		display("Usage (options are not case-sensitive):");
		display("	-ATFFile=? \t (Mandatory) Path to the ATF test script file");
		display("	-DevKey=? \t (Mandatory) Development key provided by TestLink");
		display("	-ServerURL=? \t (Mandatory) TestLink xml-rpc server URL (e.g. http://orwell/testlink/lib/api/xmlrpc.php)");
		display("	-LoginName=? \t (Mandatory) TestLink login name");
		display("	-ConfirmBeforeExport=? \t (Optional) true/false. Default true. If true, it will ask for confirmation before exporting to TestLink");
		display("	-CancelExportIfError=? \t (Optional) true/false. Default true. If true, export will not be done if there's data error");
		display("	-ProjectName=? \t (Optional) The test project name. If not stated, it will read project name from ATF File");
		display("	-TestSuiteName=? \t (Optional) The test suite name. If not stated, it will read test suite name from ATF File");
		display("	-ATFOverideTestSuiteName=? \t (Optional) True/false. Default true. If true, test suite name read from ATF file will overide the one given in the <TestSuiteName> option");
		display("	-LogFile=? \t (Optional) Application will write events log into this file");
		display("");
    }    
    
    private static void processArgs(String[] args) {
    	
    	argVal = getArg(args, "atffile", ""); //** mandatory
		if(argVal.equals("")) {
			educate("Option ATFFile is mandatory");
			System.exit(1);
		} else {
			atfFile = argVal;
		}
		
		argVal = getArg(args, "devkey", ""); //** mandatory
		if(argVal.equals("")) {
			educate("Option DevKey is mandatory");
			System.exit(1);
		} else {
			devKey = argVal;
		}
		
		argVal = getArg(args, "loginname", ""); //** mandatory
		if(argVal.equals("")) {
			educate("Option LoginName is mandatory");
			System.exit(1);
		} else {
			loginName = argVal;
		}
		
		argVal = getArg(args, "serverURL", ""); //** mandatory
		if(argVal.equals("")) {
			educate("Option ServerURL is mandatory");
			System.exit(1);
		} else {
			serverURL = argVal;
		}
		
		argVal = getArg(args, "ConfirmBeforeExport", "true");
		if(argVal.equalsIgnoreCase("true")) {
			isShowConfirm = true;
		} else if(argVal.equalsIgnoreCase("false")) {
			isShowConfirm = false;
		} else {
			educate("Option ConfirmBeforeExport only takes value TRUE or FALSE");
			System.exit(1);
		}
		
		argVal = getArg(args, "CancelExportIfError", "true");
		if(argVal.equalsIgnoreCase("true")) {
			proceedExportIfHaveError = false;
		} else if(argVal.equalsIgnoreCase("false")) {
			proceedExportIfHaveError = true;
		} else {
			educate("Option CancelExportIfError only takes value TRUE or FALSE");
			System.exit(1);
		}
		
		argVal = getArg(args, "projectname", ""); 
		if(!argVal.equals("")) {
			projName = argVal;			
		} 
		
		argVal = getArg(args, "LogFile", ""); 
		if(!argVal.equals("")) {
			logFile = argVal;			
		}
		
		argVal = getArg(args, "TestSuiteName", ""); 
		if(!argVal.equals("")) {
			suiteName = argVal;			
		}
		
		argVal = getArg(args, "ATFOverideTestSuiteName", "true");
		if(argVal.equalsIgnoreCase("true")) {
			ATFOverideTestSuiteName = true;
		} else if(argVal.equalsIgnoreCase("false")) {
			ATFOverideTestSuiteName = false;
		} else {
			educate("Option ATFOverideTestSuiteName only takes value TRUE or FALSE");
			System.exit(1);
		}
		
		argVal = getArg(args, "TestCaseTableWidth", ""); 
		if(!argVal.equals("")) {
			String[] strFormat;
			strFormat = argVal.split(",");
			if(strFormat.length == 6) {
				for(int i = 0; i < strFormat.length; i++) {
					if(Integer.parseInt(strFormat[i]) <= 0) {
						educate("Option TestCaseTableWidth only take value > 0");
						System.exit(1);
					}
				}
				indexColumnTrunc = Integer.parseInt(strFormat[0]);
				titleColumnTrunc = Integer.parseInt(strFormat[1]);
				summaryColumnTrunc = Integer.parseInt(strFormat[2]);
				suiteColumnTrunc  = Integer.parseInt(strFormat[3]);
				execColumnTrunc = Integer.parseInt(strFormat[4]);
				noStepColumnTrunc = Integer.parseInt(strFormat[5]);
			}
			else {
				educate("Option TestCaseTableWidth must have 6 values separated by comma e.g. -TestCaseTableWidth=5,10,40,90,35,5");
				System.exit(1);
			}
		}
		
		argVal = getArg(args, "TestStepTableWidth", ""); 
		if(!argVal.equals("")) {
			String[] strFormat;
			
			strFormat = argVal.split(",");
			if(strFormat.length == 4) {
				for(int i = 0; i < strFormat.length; i++) {
					if(Integer.parseInt(strFormat[i]) <= 0) {
						educate("Option TestStepTableWidth only take value > 0");
						System.exit(1);
					}
				}
				tsIndexColumnTrunc = Integer.parseInt(strFormat[0]);
				stepColumnTrunc = Integer.parseInt(strFormat[1]);
				expectedColumnTrunc = Integer.parseInt(strFormat[2]);				
				tsExecColumnTrunc = Integer.parseInt(strFormat[3]);				
			}
			else {
				educate("Option TestCaseTableWidth must have 4 values separated by comma e.g. -TestStepTableWidth=5,80,85,10");
				System.exit(1);
			}
		}
    }    
    
    private static void showIntro() {
    	display("*********************************************************");
		display("*	ATF -> TestLink Test Case Exporter Tool				*");
		display("*********************************************************");
		display("This is your options:");
		display("	ATFFile: " + atfFile);
		display("	DevKey: " + devKey);
		display("	ServerURL: " + serverURL);
		display("	LoginName: " + loginName);
		display("	ConfirmBeforeExport: " + isShowConfirm);
		display("	CancelExportIfError: " + (!proceedExportIfHaveError)); //** boolean inversion is intentional
		display("	ProjectName: " + projName);
		display("	LogFile: " + logFile);
		display(" 	TestSuiteName: " + suiteName);
		display("	ATFOverideTestSuiteName: " + ATFOverideTestSuiteName);
		display("");
    }
    
    private static void display(String message) {
    	
    	System.out.println(message);
    	if(!logFile.equals("")) {
    		EventWriter writer = new EventWriter();
    		writer.writeToEventFile(logFile, message, true);
    	}
    }       
    
    private static void showPreExportSummary(ATFReader reader, Vector<ATFTestCase> tcList) {
    	
    	int errorCount = 0;					
				
		boolean displaying = true;
		
		while(displaying) {
			ColumnPrinter mcp = new ColumnPrinter(6, tablePadding, "-", ColumnPrinter.LEFT, false);
			String    oneRow[] = new String [ 6 ];
			display("");
			display("Pre-export Summary:");
			display("-----------------------------------------------------------------");
			display(" ATF File		   : " + reader.getATFPath());
			display(" Login Name		   : " + reader.getLoginName());
			display(" Server URL		   : " + serverURL);
			display(" Project Name      : " + reader.getProjName());
			display(" Total test case(s): " + tcList.size());
			display("");
			display("List of Test Cases:");
			
			oneRow[0] = "Index";
			oneRow[1] = "Title";
			oneRow[2] = "Summary";
			oneRow[3] = "Test Suite";
			oneRow[4] = "ExecType";
			oneRow[5] = "# Step";
			mcp.addTitle(oneRow);
			
			for(int i=0; i<tcList.size(); i++) {
				if(tcList.get(i).verifyTestCase() < 0) {
					errorCount++;
					String errorDesc = "";
					switch(tcList.get(i).verifyTestCase()) {
						case -1: 
							errorDesc = "missing proj.Name";
							break;
						case -2:
							errorDesc = "missing suite name";
							break;
						case -3:
							errorDesc = "missing case name";
							break;
						case -4:
							errorDesc = "missing case summary";
							break;
						case -5:
							errorDesc = "missing login name";
							break;
						case -6:
							errorDesc = "missing exec.Type";
							break;
						case -7:
							errorDesc = "invalid test suite name format";
							break;
					}
					
					oneRow[0] = "" + (i + 1) + " ERROR:[" + errorDesc + "]";
					oneRow[1] = tcList.get(i).getTestCaseName();
					oneRow[2] = tcList.get(i).getTestCaseSummary();
					oneRow[3] = tcList.get(i).getSuiteName();
					oneRow[4] = tcList.get(i).getExecType();
					oneRow[5] = "" + tcList.get(i).getStepList().size();
					mcp.add(oneRow);
				}
				else {					
					oneRow[0] = "" + (i + 1);
					oneRow[1] = tcList.get(i).getTestCaseName();
					oneRow[2] = tcList.get(i).getTestCaseSummary();
					oneRow[3] = tcList.get(i).getSuiteName();
					oneRow[4] = tcList.get(i).getExecType();
					oneRow[5] = "" + tcList.get(i).getStepList().size();
					int truncLimit[] = {indexColumnTrunc, titleColumnTrunc, summaryColumnTrunc, suiteColumnTrunc, execColumnTrunc, noStepColumnTrunc};
					Vector<String[]> rowList = new Vector<String[]>();
					rowList = prepareTruncateColumn(oneRow, truncLimit);
					for(int j = 0; j < rowList.size(); j++) {
						oneRow = rowList.get(j);
						mcp.add(oneRow);
					}
					//mcp.add(oneRow);
				}
			}
			mcp.print();
					
			display("");
			
			//** no test case found
			if(tcList.size() == 0) {
				display("No test case found");
				display("");
				System.exit(0);
			}			
			
			display("Error found: " + errorCount);
			display("");
			
			//** all test cases got error, no valid test case to export
			if(errorCount == tcList.size()) {
				display("All test case(s) have data error. No valid test case to export");
				display("");
				System.exit(0);
			}
			
			if(!proceedExportIfHaveError && errorCount > 0) {
				display("Export will not proceed since test case has error.");
				display("");
				System.exit(1);
			}
			
			if(isShowConfirm) {
				display("Proceed with export? type [yes] if want to proceed with export. Type index no [1-" + tcList.size() +"] to see test case details:");
				try {
					String keyPress = keyboard.readLine();
					if(isInteger(keyPress)) {
						if((Integer.parseInt(keyPress) > tcList.size()) || (Integer.parseInt(keyPress) < 1)) {
							displaying = true;
							continue;
						}
						showTCDetails(tcList, Integer.parseInt(keyPress) - 1);
						displaying = true;
						continue;
					}
					else if(! keyPress.equalsIgnoreCase("yes")) {
						display("Exited because user opted not to proceed with export.");
						System.exit(0);
					}
					displaying = false;
					break;
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}
		}
    }    
    
    private static void showTCDetails(Vector<ATFTestCase> tcList, int tcIndex) {
    	
    	ColumnPrinter mcp = new ColumnPrinter(4, tablePadding, ".", ColumnPrinter.LEFT, false);
		String oneRow[] = new String [ 4 ];	
    	
    	display("");
    	display("Test Case details (Index: " + (tcIndex + 1) + ")");
    	display("-----------------------------------------------------------");
    	display("Title         : " + tcList.get(tcIndex).getTestCaseName());
    	display("Summary       : " + tcList.get(tcIndex).getTestCaseSummary());
    	display("Test Suite    : " + tcList.get(tcIndex).getSuiteName());
    	display("Execution Type: " + tcList.get(tcIndex).getExecType());
    	display("");
    	if(tcList.get(tcIndex).getStepList().size() == 0) {
    		display("This test case has no test step");    		  		
    	}
    	else {
    	display("Test Step(s):");
    	display("");
    	
			oneRow[0] = "No";
			oneRow[1] = "Step Action";
			oneRow[2] = "Expected Result";		
			oneRow[3] = "Execution";
			mcp.addTitle(oneRow);
			
			for(int i = 0; i < tcList.get(tcIndex).getStepList().size(); i++) {
								
				oneRow[0] = "" + (i + 1);
				oneRow[1] = tcList.get(tcIndex).getStepList().get(i).getStep();
				oneRow[2] = tcList.get(tcIndex).getStepList().get(i).getExpected();
				oneRow[3] = tcList.get(tcIndex).getStepList().get(i).getExecType();
				int truncLimit[] = {tsIndexColumnTrunc, stepColumnTrunc, expectedColumnTrunc, tsExecColumnTrunc};
				Vector<String[]> rowList = new Vector<String[]>();
				rowList = prepareTruncateColumn(oneRow, truncLimit);
				for(int j = 0; j < rowList.size(); j++) {
					oneRow = rowList.get(j);
					mcp.add(oneRow);
				}				
			}		
			
			mcp.print();
    	}
    	
		display("");
		display("Press any key to continue...");
		try {
			String keyPress = keyboard.readLine();
			keyPress = keyPress.trim(); //** no use			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		display("");
		return;
    }
    
    private static Vector<String[]> prepareTruncateColumn(String val[], int truncLimit[]) {
    	
    	Vector<String[]> rowList = new Vector<String[]>();
    	int rowNeeded = 0;
    	int maxRow = 0;
    	String row[] = {};
    	int startPos = 0;
    	int endPos = 0;
    	
    	//** check if any of the column need to be trancated
    	for(int i = 0; i < val.length; i++) {
    		rowNeeded = (int) Math.ceil(((float)val[i].length() / truncLimit[i]));
    		if(rowNeeded > maxRow)
    			maxRow = rowNeeded;
    	}
    	
    	for(int i = 0; i < maxRow; i++) {
    		row = new String[val.length];
    		for(int j = 0; j < val.length; j++) {  
    			
    			startPos = truncLimit[j] * i;
    			endPos = truncLimit[j] * i + truncLimit[j];
    			
    			if(startPos > val[j].length()) {
    				row[j] = "";
    				continue;
    			}
    			if(endPos > val[j].length())
    				row[j] = val[j].substring(startPos, val[j].length());
    			else
    				row[j] = val[j].substring(startPos, endPos); 
    		} 
    		rowList.addElement(row);
    	}
    	
    	return rowList;
    }
    
    private static boolean isInteger( String input )  
    {  
       try  
       {  
          Integer.parseInt( input );  
          return true;  
       }  
       catch( Exception e)  
       {  
          return false;  
       }  
    }
    
    
    private static int getErrorTCCount(Vector<ATFTestCase> tcList) {
    	
    	int errorCount = 0;
    	
    	for(int i=0; i<tcList.size(); i++) {
			if(tcList.get(i).verifyTestCase() < 0) 
				errorCount++;
    	}
    	
    	return errorCount;
    }    
    
    private static List<TestCaseStep> convertStepList(Vector<ATFTestStep> stepList) {
    	
    	List<TestCaseStep> tcsList = new Vector<TestCaseStep>();
    	
    	for(int i = 0; i < stepList.size(); i++) {
    		TestCaseStep tcs = new TestCaseStep();
    		tcs.setActions(stepList.get(i).getStep());
    		tcs.setExpectedResults(stepList.get(i).getExpected());
    		if(stepList.get(i).getExecType().equalsIgnoreCase("AUTOMATED"))
    			tcs.setExecutionType(ExecutionType.AUTOMATED);
    		
    		tcs.setActive(true);
    		tcs.setId(i+1);
    		tcs.setNumber(i+1);
    		tcsList.add(tcs);
    	}
    	
    	return tcsList;
    }
    	
	public static void main(String[] args) {
		
		int status;
		int projID;
		int suiteID;		
		int caseId = 0;
		
		processArgs(args);
		
		Vector<ATFTestCase> tcList;		
		TestLinkClient client = new TestLinkClient();
		
		showIntro();		
		
		//*** Read from ATF
		ATFReader reader = new ATFReader(atfFile, projName, loginName, suiteName, ATFOverideTestSuiteName);
		tcList = reader.readTestCases();		
		
		showPreExportSummary(reader, tcList);
		
		//** no test case found
		if(tcList.size() == 0) {
			display("No test case found");
			display("");
			System.exit(0);
		}
		
		//** all test cases got error, no valid test case to export
		if(getErrorTCCount(tcList) == tcList.size()) {
			display("All test case(s) have data error. No valid test case to export");
			display("");
			System.exit(0);
		}
		
		status = client.connect(serverURL, devKey);
		if(status < 0) {
			display("Error connecting to the TestLink server. Check server URL & DevKey");
			System.exit(1);
		}
		
		
		//******************* Below this is the export process
		int totalTCCreated = 0;
		int totalTCExist = 0;
		int totalTCError = 0;
		int totalTCNotExp = 0;
		int planId = 0;				
		
		projID = client.getProjectID(reader.getProjName());
		for(int i=0; i<tcList.size(); i++) {
			ATFTestCase tc = tcList.get(i);
			if(tc.verifyTestCase() < 0) { //** error in the test case data
				totalTCError++;
				display("Test Case [" + (i + 1) + "] has data error. This test case is NOT exported");
				continue;
			}
			display("Exporting Test Case " + (i+1) + "/" + tcList.size() + ": [" + tc.getTestCaseName() + "] [Test Suite: " + tc.getSuiteName() + "]");
			
			Vector<String> suiteHier = new Vector<String>();
			suiteHier = reader.loadSuiteHierarchy(tc.getSuiteName());
			suiteID = client.getTestSuiteID(projID, suiteHier, createSuiteIfNotExist);
			if(suiteID > 0) {
				caseId = client.createTestCase(tc.getTestCaseName(), suiteID, projID, tc.getLoginName(), tc.getTestCaseSummary(), tc.getExecType(), "ALWAYS_APPEND", convertStepList(tc.getStepList()));				
				if(caseId > 0) {
					totalTCCreated++;
					display("\tTest case " + (i+1) + " successfully created");
					//**** start temp
					String platformName = "HP-UX";
					Integer platformId = 0;
					planId = client.getProjectPlanByName(projID, "Sprint - OSA ASCII Parser Plugin");
					display("Plan id: " + planId);
					if(planId == -2) { //*** test plan not found
						display("Test plan not found, creating one");
						//planId = client.createTestPlan("test plan test2", reader.getProjName());
						planId = -1; // temp
					}
					
					if(planId > 0) {
						platformId = client.getPlatform(planId, platformName);
						status = client.addTestCaseToTestPlan(projID, planId, caseId, platformId, 0);
						display("Status: " + status);
						if(status > 0) {
							display("Test case has been added to test plan sucessfully");
						} else {
							display("adding test case to test plan has FAILED. Failed at 'addTestCaseToTestPlan'");
						}
					} else {
						display("adding test case to test plan has FAILED. Reason: failed to create a test plan");
					}
					//**** end temp
				} else if(caseId == -1){
					totalTCExist ++;
					display("\tTest case " + (i+1) + " creation failed. Test case already exists");
				} else {
					totalTCNotExp++;
					display("\tgeneral error when trying to create test case");
				}
					
			} else {
				totalTCNotExp++;
				if(caseId == -2)
					display("Test Suite already exist when trying to create one");
				else
					display("Error when try to get/create the test suite ID");
			}
		}
		
		display("Export done");
		display("");
		display("Post-export summary");
		display("----------------------------------");
		display("Total test case exported: " + totalTCCreated);
		display("Total test case NOT exported due to test case already exist: " + totalTCExist);
		display("Total test case NOT exported due to data error: " + totalTCError);
		display("Total test case NOT exported due to test suite reference error:" + totalTCNotExp);
		display("");
	}
	
	

}
