package com.openet.testlinkclient;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventWriter {
	
	public int writeToEventFile(String file, String data, boolean writeTimeStamp)
    {
        FileWriter fwriter = null;
        BufferedWriter out = null;
        try {
            fwriter = new FileWriter(file, true);
            out = new BufferedWriter(fwriter);
            if(writeTimeStamp) {
            	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            	data = dateFormat.format(new Date()) + " " + data;
            }
            out.write(data + "\n");
        } catch(IOException e) {
            e.printStackTrace();
            return -1;
        } finally {
            try {
                out.close();
            } catch(IOException e) {
                ;
            }
        }
        
        return 0;

    }


}
