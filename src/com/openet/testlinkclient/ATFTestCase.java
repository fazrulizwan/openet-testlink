package com.openet.testlinkclient;

import java.util.Vector;

public class ATFTestCase {
	
	private String suiteName = "";
	private String testCaseName = "";
	private String testCaseSummary = "";
	private String loginName = "";
	private String execType = "";
	private Vector<ATFTestStep> stepList = new Vector<ATFTestStep>();
	
	public final String AUTOMATED = "AUTOMATED";
	public final String MANUAL = "MANUAL";
	
	public ATFTestCase(String suiteName, String testCaseName, String testCaseSummary, String execType, String loginName) {		
		this.suiteName = suiteName;
		this.testCaseName = testCaseName;
		this.testCaseSummary = testCaseSummary;
		this.execType = execType;
		this.loginName = loginName;
	}
	
	public ATFTestCase() {
		
	}
		
	/*
	 * Self-verification
	 * Return 0: if test case is valid
	 * 		-1: no proj name
	 * 		-2: no suite name
	 * 		-3: no case name
	 * 		-4: no case summary
	 * 		-5: no login name
	 * 		-6: no exectype
	 */
	public int verifyTestCase() {
		
		//if(projName.equals("")) //** projname no longer an attribute to test case, the return set remains
		//	return -1;
		if(suiteName.equals(""))
			return -2;
		if(testCaseName.equals(""))
			return -3;
		if(testCaseSummary.equals(""))
			return -4;
		if(loginName.equals(""))
			return -5;
		if(execType.equals(""))
			return -6;
		if(verifyTestSuiteName(suiteName) == -1)
			return -7;
		
		return 0;
	}	
	
	private int verifyTestSuiteName(String suiteName) {
		
		/*
		 * check syntax of suiteName 
		 */
		
		String[] token = suiteName.split("/");
		for(int i = 0; i < token.length; i++) {
			token[i] = token[i].trim();
    		token[i] = token[i].replaceAll("\\s+", " ");
    		token[i] = token[i].replaceAll("[\\t]", "");
			if(token[i].equals(""))
				return -1;
		}
		
		return 0;
	}
	
	public void setSuiteName(String val) { suiteName = val; }
	public void setTestCaseName(String val) { testCaseName = val; }
	public void setTestCaseSummary(String val) { testCaseSummary = val.trim(); }
	public void setExecType(String val) { execType = val; }
	public void setLoginName(String val) { loginName = val; }	
	public void setStepList(Vector<ATFTestStep> val) { stepList = val; }
	
	public String getSuiteName() { return suiteName; }
	public String getTestCaseName() { return testCaseName; }
	public String getTestCaseSummary() { return testCaseSummary; }
	public String getExecType() { return execType; }
	public String getLoginName() { return loginName; }
	public Vector<ATFTestStep> getStepList() { return stepList; }	
}
