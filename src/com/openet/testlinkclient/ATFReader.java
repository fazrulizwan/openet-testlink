package com.openet.testlinkclient;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

public class ATFReader {
	
	private String atfPath = "";
	private String projName = "";
	private String loginName = "";
	private String suiteName = "";
	private boolean ATFOverideTestSuiteName;
	
	public ATFReader(String atfPath, String projName, String loginName, String suiteName, boolean ATFOverideTestSuiteName) {
		this.atfPath = atfPath;
		this.projName = projName;
		this.loginName = loginName;
		this.suiteName = suiteName;
		this.ATFOverideTestSuiteName = ATFOverideTestSuiteName;
	}
	
	public String getProjName() { return projName; }
	public String getATFPath() { return atfPath; }
	public String getLoginName() { return loginName; }
	
	private String readProjName(Vector<String> lineList) {
		String param = "";
		for(int i = 0; i < lineList.size(); i++) {
			param = getTestCaseParam(lineList.get(i).toLowerCase());
			if(param.equalsIgnoreCase("projname"))
				return getTestCaseParamValue(lineList.get(i));
		}
		
		return "";
	}
	
	public Vector<ATFTestCase> readTestCases() {
		
		Vector<ATFTestCase> tcList = new Vector<ATFTestCase>();
		ATFTestCase tc = new ATFTestCase();
		String line;		
		Vector<String> lineList = new Vector<String>();
		BufferedReader reader = null;
		
		try {
			FileInputStream file = new FileInputStream(atfPath);	
			InputStreamReader inReader = new InputStreamReader(file);
			reader = new BufferedReader(inReader);
			while((line = reader.readLine()) != null) {
				//System.out.println("line "  + line);
				lineList.addElement(line);				
			}
			
			if(projName.equals("")) //** if no proj name defined in option
				projName = readProjName(lineList); //** find it in the ATF file
				
			for(int i = 0; i < lineList.size(); i++) {
				line = lineList.get(i);
				if(isProcHeader(line)) {
					//System.out.println("Line " + (i+1) + " is proc header: " + line);
					tc = readTestCase(lineList, i);
					tcList.addElement(tc);
				}
			}
			
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}
		
		return tcList;
	}
	
	private ATFTestCase readTestCase(Vector<String> lineList, int procPos) {
		ATFTestCase tc = new ATFTestCase();
		boolean keepLooking = true;
		String line = null;
		int lineIndex = procPos - 1; //** step one line upper than the proc header
		boolean isCommentCloseFound = false;
		boolean isCommentOpenFound = false;
		boolean isTestCaseFound = false;
		Vector<String> stepTokenList = new Vector<String>();
		ATFTestStep testStep = new ATFTestStep();
		Vector<ATFTestStep> stepList = new Vector<ATFTestStep>();
		
		while(keepLooking) {
			line = lineList.get(lineIndex);
			if((isCommentCloseFound == false) && (isCommentBorderFound(line) == true)) //** found the close comment block
				isCommentCloseFound = true;
			else if((isCommentCloseFound == true) && isCommentBorderFound(line) == true) //** foundt the open comment block
				isCommentOpenFound = true;
			
			if((isCommentCloseFound == true) && (isCommentOpenFound == true)) { //** if whole comment block traversed, finish it
				keepLooking = false;
				continue;
			}
			
			String tcParam = getTestCaseParam(line);
			if(tcParam.equalsIgnoreCase("name")) {
				tc.setTestCaseName(getTestCaseParamValue(line));
				isTestCaseFound = true;
			}
			else if(tcParam.equalsIgnoreCase("purpose")) {
				tc.setTestCaseSummary(getTestCaseParamValue(line) + " " + getExtendedParamValue(lineList, lineIndex));
				isTestCaseFound = true;
			}
			else if(tcParam.equalsIgnoreCase("suite")) {
				if(!ATFOverideTestSuiteName) //** if ATF dont overide option, then use value from the option
					tc.setSuiteName(suiteName);
				else						//** if ATF overide option, then use value from ATF
					tc.setSuiteName(getTestCaseParamValue(line));				
				isTestCaseFound = true;
			}
			else if(tcParam.equalsIgnoreCase("step")) {
				String stepVal = "";
				
				
				stepVal = getTestCaseParamValue(line) + " " + getExtendedParamValue(lineList, lineIndex);				
				stepTokenList = separateStepToken(stepVal);
				if(stepTokenList.size() > 0) { //** create a step instance only if there's valid step
					testStep = convertStepTokenIntoTestStep(stepTokenList);
					testStep.setExecType("AUTOMATED");
					stepList.add(0, testStep); //** insert as the top of the list because we track the comment block upside down
					tc.setStepList(stepList);
				}
			}
			
			lineIndex--;
			
			//** validation
			if(lineIndex == -1)
				break;
		}
		
		if(isTestCaseFound) { //** put the rest of the properties			
			if((!suiteName.equals("")) && (tc.getSuiteName().equals(""))) { //** if user has set suite name in the option AND 
				tc.setSuiteName(suiteName);	//** no suite name found in the ATF, then set using the option value
			}
			tc.setExecType(tc.AUTOMATED);
			tc.setLoginName(loginName);
		}
		
		return tc;
	}
	
	
	private ATFTestStep convertStepTokenIntoTestStep(Vector<String> stepTokenList) {
		
		ATFTestStep testStep = new ATFTestStep();
		String expected = "";		 
		
		if(stepTokenList.size() == 0)
			return testStep;
		
		testStep.setStep(stepTokenList.get(0)); //** 0 index always the step action
		for(int i = 1; i < stepTokenList.size(); i++) { //** the subsequent index is the result(s)
			if(i > 1)
				expected = expected + " <br><br>"; //** add carriage return to multiple expected
			expected = expected + i + ") " + stepTokenList.get(i);
			
		}
		testStep.setExpected(expected);	
		
		return testStep;
	}
	
	
	private Vector<String> separateStepToken(String stepVal) {
		/*
		 * given the step val e.g. "Step 1 bla bla" / expected 1a / expected 1b
		 * and load the token into list e.g. 
		 * 		1. Step 1 bla bla
		 * 		2. expected 1a
		 * 		3. expected 1b
		 */
		
		int openCaretPos = 0;
		int closeCaretPos = 0;
		int slashStartPos = 0;		
		int slashEndPos = 0;
		Vector<String> tokenList = new Vector<String>();
		boolean keepDoing = true;
		
		stepVal = stepVal.trim();
		stepVal = stepVal.replaceAll("[\\t]", "");
		
		if(stepVal.equals(""))
			return tokenList;
		
		while(keepDoing) {
			
			if(openCaretPos >= stepVal.length()) //** guard if the syntax is not perfect
				return tokenList;
						
			//*** get the quoated token both " or '
			if((stepVal.charAt(openCaretPos) == '"') || ((int) stepVal.charAt(openCaretPos) == 39)) {
				if(stepVal.charAt(openCaretPos) == '"')
					closeCaretPos = stepVal.indexOf(34, openCaretPos + 1);
				else if((int) stepVal.charAt(openCaretPos) == 39)
					closeCaretPos = stepVal.indexOf(39, openCaretPos + 1);
				if(closeCaretPos > 0) {
					slashStartPos = stepVal.indexOf("/", closeCaretPos + 1);
					if(slashStartPos < 0) { //** let's just look for more /.. if no take everthing as token
						if(closeCaretPos == stepVal.length() - 1)
							tokenList.addElement(stepVal.substring(openCaretPos + 1, closeCaretPos).trim());
						else
							tokenList.addElement(stepVal.substring(openCaretPos + 1, stepVal.length()).trim());
						return tokenList;
					}
					else {
						tokenList.addElement(stepVal.substring(openCaretPos + 1, closeCaretPos).trim());
					}
				} else { //** no close quote found. this could be from invalid syntax
					return tokenList;
				}
				
				//** find the next / after the previous token (if any)
				slashStartPos = stepVal.indexOf("/", closeCaretPos + 1);
				if(slashStartPos < 0) //** no more slash found, assume no more token to find
					return tokenList;
				for(int i=slashStartPos + 1; i < stepVal.length(); i++) { //** find the first non-space char after the slash found
					if(stepVal.charAt(i) != ' ') {
						openCaretPos = i;
						break;
					}
				}
				
			} 
			else { //** this token is not quoted
				slashEndPos = stepVal.indexOf("/", openCaretPos);
				if(slashEndPos < 0) { //** no close slash found, assume token until the end
					tokenList.addElement(stepVal.substring(openCaretPos, stepVal.length()).trim());
					return tokenList;
				}
				
				tokenList.addElement(stepVal.substring(openCaretPos, slashEndPos));
				openCaretPos = slashEndPos + 1;
				//** temp test
				for(int i=slashEndPos + 1; i < stepVal.length(); i++) { //** find the first non-space char after the slash found
					if(stepVal.charAt(i) != ' ') {
						openCaretPos = i;
						break;
					}
				}
			}
			
			if(openCaretPos > stepVal.length()) //** guard
				keepDoing = false;
		}
		
				
		return tokenList;
	}
	
	/*
	 * return true if the line is either comment open or comment close line.
	 * assuming that ### is the minimal criteria
	 */
	private boolean isCommentBorderFound(String line) {
		
		line = line.trim().replaceAll("\\s+", " ");
		
		if(line.length() < 3) return false;
		if(line.substring(0, 3).equalsIgnoreCase("###"))
			return true;
		
		return false;
	}
	
	/*
	 *  getTestCaseParam: given a line, it will return whether this
	 *  line is a valid parameter or not. If valid, it will return
	 *  the param field. If not valid it will return null
	 *  
	 */
	private String getTestCaseParam(String line) {		
		
		line = line.trim().replaceAll("\\s+", " ");
		String lineCopy = line.toLowerCase();
		if(line.equals("")) return "";
		if(line.charAt(0) == '#') {
			if(lineCopy.contains("test project name:"))
				return "projname";
			if(lineCopy.contains("name:"))
				return "name";
			if(lineCopy.contains("purpose:"))
				return "purpose";
			if(lineCopy.contains("suite:"))
				return "suite";
			if(lineCopy.contains("step:"))
				return "step";	
		}
		
		return "";
	}
	
	/*
	 * getTestCaseParamValue: it takes the value portion of the parameter line
	 * e.g. input: "Name: TEST_CASE001" output: "TEST_CASE001"
	 */
	private String getTestCaseParamValue(String line) {
		
		int startPos = 0;
		int endPos = 0;
		String paramValue = "";
		
		String lineCopy = line.trim().replaceAll("\\s+", " ");			
				
		startPos = lineCopy.indexOf(":") + 1;
		endPos = lineCopy.length();
		paramValue = lineCopy.substring(startPos, endPos);		
		return paramValue.trim();
						
	}
	
	/*
	 * To get the extended param value, e.g. param value in another line
	 *  paramPos should be where the param field is found e.g. "name:" 
	 */
	private String getExtendedParamValue(Vector<String> lineList, int paramPos) {
		
		boolean keepLooking = true;
		String retval = "";
		
		paramPos ++;
		
		while(keepLooking) {
			String line = lineList.get(paramPos);
			line = line.replaceAll("[\\t]", "");
			line = line.trim().replaceAll("\\s+", " ");
			
			
			//** exit criteria:
			if(line.equals("#")) //** it's an empty comment line
				return retval;
			if(line.equals("")) //** it's a new non-comment line			
				return retval;
			if(line.charAt(0) != '#') //** it's not a comment at all
				return retval;
			//** if it's a comment and there's colon
			//** we can assume it's another new parameter
			if((line.charAt(0) == '#') && (line.indexOf(':') > 0)) { //**todo: need more robust approach
				if(isLineParameterHeader(lineList.get(paramPos))) //** pass original line instead of trimmed one
					return retval;
			}
			
			//** below this line we confirm the line is valid extended param value
			line = line.replace("#", " "); //** easy way to get the value
			retval = retval + line.trim() + " ";
			paramPos++;
			
			//** limit guard
			if(paramPos >= lineList.size())
				return retval;
		}
		
		return retval;
		
	}
	
	private boolean isLineParameterHeader(String line) {
		/*
		 * analyze a line and tell whether the line has a parameter header
		 * e.g. "# Parameters:" is a parameter header while
		 * return true if line has parameter header
		 * 
		 * best if line passed is not trim
		 */
		
		int colonLocation = 0;
		String testStr = "";
		int spaceCount = 0;
		
		line = line.replaceAll("[\\t]", "    "); //** replace tab with 4 spaces so we get close as what the eyes see
		
		//** get the string up to : location
		colonLocation = line.indexOf(":", 0);
		testStr = line.substring(0, colonLocation);
		
		if(testStr.equals("")) //** guard
			return false;
		
		//** count no of spaces in the string up to colon location
		for(int i = 0; i < testStr.length(); i++) {
			if(testStr.charAt(i) == ' ')
				spaceCount++;
			if(spaceCount > 4) //** assume to many spaces between the start of line and colon, writer doesn't mean a parameter
				return false;
		}
		
		return true;
	}
	
	private boolean isProcHeader(String line) {
		
		line = line.trim().replaceAll("\\s+", " ");
		line = line.replaceAll("[\\t]", "");
		if(line.length() < 9) return false;		
		if(line.substring(0, 9).equals("proc Test"))			
			return true;
		if(line.substring(0, 10).equals("proc aTest"))			
			return true;
		
		return false;
	}	
	
	public Vector<String> loadSuiteHierarchy(String suiteNameVal) {
    	
    	/*
    	 * to split suitenames from "a / b / c" format and load into list 
    	 * 
    	 */
    	
    	Vector<String> suiteHier = new Vector<String>();
    	String[] token = suiteNameVal.split("/");
    	for(int i = 0; i < token.length; i++) {
    		token[i] = token[i].trim();
    		token[i] = token[i].replaceAll("\\s+", " ");
    		token[i] = token[i].replaceAll("[\\t]", "");
    		suiteHier.addElement(token[i]);
    	}
    	    	
    	return suiteHier;
    	
    }
	
}
