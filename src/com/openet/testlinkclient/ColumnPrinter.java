package com.openet.testlinkclient;

public class ColumnPrinter extends MultiColumnPrinter {

	public ColumnPrinter(int numCol, int gap, String border, 
            int align, boolean sort) {
		super(numCol, gap, border, align, sort);
		// TODO Auto-generated constructor stub
	}
	
	

	@Override
	public void doPrint(String str) {
		System.out.print(str);		
	}

	@Override
	public void doPrintln(String str) {
		System.out.println(str);
		
	}

}
