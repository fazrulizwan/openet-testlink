package com.openet.testlinkclient;

public class ATFTestStep {

	private String step = "";
	private String expected = "";
	private String execType ="";
	
	public ATFTestStep() {
		
	}
	
	public void setStep(String val) { step = val; }
	public void setExpected(String val) { expected = val; }
	public void setExecType(String val) {execType = val; }
	
	public String getStep() { return step; }
	public String getExpected(){ return expected; }
	public String getExecType(){ return execType; }
}
